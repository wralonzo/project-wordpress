<?php

// CSS Switcher
add_action( 'customize_register', 'fonts_customize_register' );
function fonts_customize_register( $wp_customize ) {

    // Load the radio image control class.
    //require_once( trailingslashit( get_template_directory() ) . 'inc/assets/css-switcher/control-radio-image.php' );

    // Register the radio image control class as a JS control type.
    //$wp_customize->register_control_type( 'Customize_Control_Radio_Image' );

  // Register CSS Switcher Section
    $wp_customize->add_section('fonts_switcher_section', array(
        'title'    => __('Font Selector', 'nombre'),
        'priority' => 40, //Above style options
    ));


    $wp_customize->add_setting( 'preset_style_setting', array(
        'default'   => 'default',
        'type'       => 'theme_mod',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'preset_style_setting', array(
        'label' => __( 'Typography', 'wp-bootstrap-starter' ),
        'section'    => 'fonts_switcher_section',
        'settings'   => 'preset_style_setting',
        'type'    => 'select',
        'choices' => change_fonts()
    ) ) );

}


function change_fonts()
{
    $ruta = $ruta =  dirname(__FILE__).'\assets\fonts';
    // Se comprueba que realmente sea la ruta de un directorio
    $guardar = [];
    if (is_dir($ruta)) {
        // Abre un gestor de directorios para la ruta indicada
        $gestor = opendir($ruta);
        // echo "<ul>";
        // $guardar = [];
        // Recorre todos los elementos del directorio
        while (($archivo = readdir($gestor)) !== false) {

            // Se muestran todos los archivos y carpetas excepto "." y ".."
            if ($archivo != "." && $archivo != "..") {
                // echo  str_replace('.','',$archivo).'<br>';
                $save1 =  substr($archivo, 0, -4);
                // $comparar =  substr($archivo, -3, 4) . "<br>";
                if (substr($archivo, -3, 4) == 'ttf') {
                    $guardar +=  [$save1 => $save1];
                }
                // array_push($archivo, $save1);
            }
        }
        // $guardar = array("roberto", 'Willian');

        // var_dump($guardar);

        // Cierra el gestor de directorios
        closedir($gestor);
        // echo "</ul>";
    } else {
        // echo "No es una ruta de directorio valida<br/>";
    }



    return $guardar;
}
// /Css Switcher

?>
