<?php

// CSS Switcher
add_action( 'customize_register', 'customize_register' );
function customize_register( $wp_customize ) {

    // Load the radio image control class.
    require_once( trailingslashit( get_template_directory() ) . 'inc/assets/css-switcher/control-radio-image.php' );

    // Register the radio image control class as a JS control type.
    $wp_customize->register_control_type( 'Customize_Control_Radio_Image' );

  // Register CSS Switcher Section
    $wp_customize->add_section('css_switcher_section', array(
        'title'    => __('Seleccionar Estilo (CSS)', 'nombre'),
        'priority' => 30, //Above style options
    ));

    // Add the CSS Switcher Setting (Theme option's)
    $wp_customize->add_setting('theme_option_setting', array(
            'default'   => 'default',
            'type'       => 'theme_mod',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'wp_filter_nohtml_kses',
        )
    );


     $wp_customize->add_control(
        new Customize_Control_Radio_Image(
            $wp_customize,
            'theme_option_setting',
            array(

        'label'      => __('Hojas de Estilo', 'nombre'),
        'section'    => 'css_switcher_section',
        'settings'   => 'theme_option_setting',
        'description' => __( 'Seleccionar la hoja de estilos (CSS) para su sitio web.', 'wp-bootstrap-starter' ),
                'choices'  => array(
                    'default' => array(
                        'label' => esc_html__( 'Default', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme-default.png'
                    ),
                    'cerulean' => array(
                        'label' => esc_html__( 'Cerulean', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme01.png'
                    ),
                    'cosmo' => array(
                        'label' => esc_html__( 'Cosmo', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme02.png'
                    ),
                    'cyborg' => array(
                        'label' => esc_html__( 'Cyborg', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme03.png'
                    ),
                    'darkly' => array(
                        'label' => esc_html__( 'Darkly', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme04.png'
                    ),
                    'flatly' => array(
                        'label' => esc_html__( 'Flatly', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme05.png'
                    ),
                    'journal' => array(
                        'label' => esc_html__( 'Journal', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme06.png'
                    ),
                    'litera' => array(
                        'label' => esc_html__( 'Litera', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme07.png'
                    ),
                    'lumen' => array(
                        'label' => esc_html__( 'Lumen', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme08.png'
                    ),
                    'lux' => array(
                        'label' => esc_html__( 'Lux', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme09.png'
                    ),
                    'materia' => array(
                        'label' => esc_html__( 'Materia', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme10.png'
                    ),
                    'minty' => array(
                        'label' => esc_html__( 'Minty', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme11.png'
                    ),
                    'pulse' => array(
                        'label' => esc_html__( 'Pulse  ', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme12.png'
                    ),
                    'sandstone' => array(
                        'label' => esc_html__( 'Sandstone', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme13.png'
                    ),
                    'simplex' => array(
                        'label' => esc_html__( 'Simplex', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme14.png'
                    ),
                    'sketchy' => array(
                        'label' => esc_html__( 'Sketchy', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme15.png'
                    ),
                    'slate' => array(
                        'label' => esc_html__( 'Slate', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme16.png'
                    ),
                    'solar' => array(
                        'label' => esc_html__( 'Solar', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme17.png'
                    ),
                    'spacelab' => array(
                        'label' => esc_html__( 'Spacelab', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme18.png'
                    ),
                    'superhero' => array(
                        'label' => esc_html__( 'Superhero', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme19.png'
                    ),
                    'united' => array(
                        'label' => esc_html__( 'United', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme20.png'
                    ),
                    'yeti' => array(
                        'label' => esc_html__( 'Yeti', 'jt' ),
                        'url'   => '%s/inc/assets/css-switcher/images/theme21.png'
                    ),
                )
))
    );

}
// /Css Switcher

?>
