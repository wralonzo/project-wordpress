<!-- variables php para obtener la ruta del archivo -->
<?php
    $urlsinparametros = explode('@', $_SERVER['REQUEST_URI'], 2);
    $urlsinparametros[0];
    $url = 'http://' . $_SERVER["HTTP_HOST"] . $urlsinparametros[0];
    // echo $url;
    // echo trim($url, '/');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upload Fonts</title>
    <link rel="stylesheet" href="<?php echo $url; ?>inc/assets/css/mis-estilos.css">
</head>

<body>

    <h1><strong>Subir Fuente E</strong> Instalar en el sistema</h1>
    <div class="upload-btn-wrapper">
        <form method="POST" action="@load-font.php" enctype="multipart/form-data">
            <input type="file" name="file" /><br><br>
            <button class="btn">Subir Archivo</button>
        </form>
    </div>
    <a href='<?php echo 'http://' . $_SERVER["HTTP_HOST"] . '/wordpress/wp-admin/customize.php'; ?> ' class="back-to-article">back to Article</a>
</body>

<?php


// // Cómo subir el archivo
// $url = '';
function upload_file($ruta, $file)
{
    if (isset($_FILES['file'])) {
        global $url;
        $url_padre =  trim($url, '/');
        $fichero = $_FILES[$file];
        $destinoparrafos = 'inc/assets/css/presets/typography/parrafos/';
        $destinotitulos = 'inc/assets/css/presets/typography/titulos/';
        $nombre = explode('.', $fichero["name"]);
        move_uploaded_file($fichero["tmp_name"], $ruta . $fichero["name"]);
        // Shell_exec(dirname(__DIR__) . '/wp-cs-starterplus/inc/assets/fonts/' . $fichero["name"]);
        $parrafos = fopen($destinoparrafos . $nombre[0] . '.css', 'w') or die("Se produjo un error al crear el archivo");
        $titulos = fopen($destinotitulos . $nombre[0] . '.css', 'w') or die("Se produjo un error al crear el archivo");

$textoparrafo = 
<<<_END
    @font-face {
    font-family: '$nombre[0]';
    src: url('$url_padre/inc/assets/fonts/$fichero[name]') format('opentype');
    }
    p{
    font-family: '$nombre[0]';
    }

_END;
$textotitulos =
        <<<_END
    @font-face {
    font-family: '$nombre[0]';
    src: url('$url_padre/inc/assets/fonts/$fichero[name]') format('opentype');
    }
    h1, h2, h3, h4, h5, h6{
    font-family: '$nombre[0]';
    }

_END;

        fwrite($parrafos, $textoparrafo) or die("No se pudo escribir en el archivo");

        fclose($parrafos);
        fwrite($titulos, $textotitulos) or die("No se pudo escribir en el archivo");

        fclose($titulos);

        echo "<center><span>El archivo se ha subido sin problemas</span></center>";
    }
}
// echo dirname(__DIR__);


// echo "hola;";
$ruta  = 'inc/assets/fonts/';


$file = 'file';
upload_file($ruta, $file);
// header('Location: '.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
// echo $_SERVER["HTTP_HOST"] . $_SERVER["PHP_SELF"];
