<?php // teaser simple. se deben modificar los parametros post type, tag, showpost. también se pueden cambiar por otros.
                    $args = array(
                        'post_type' => 'post',
                        'ID' => 28,
                        'showposts' => 2);
                    $category_posts = new
                        WP_Query($args);
                    if ( have_posts() ) :
                        while ( $category_posts->have_posts() ) :
                            $category_posts->the_post();
                            $thumbnail_id   = get_post_thumbnail_id();
                            $thumbnail_url  = wp_get_attachment_image_src( $thumbnail_id, 'teaser-abcf', true );
                            $thumbnail_meta = get_post_meta( $thumbnail_id, '_wp_attatchment_image_alt', true );?>
                    <div class="teaser-item teaser-b">
                        <a href="#" class="teaser-image">
                            <img class="img-responsive" src="<?php echo $thumbnail_url[0]; ?>" alt="<?php the_title(); ?>">
                        </a>
                        <h2 class="teaser-title">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h2>
                        <div class="teaser-excerpt">
                            <?php the_excerpt(); ?>
                            <a class="btn btn-outline-secondary btn-sm" href="<?php the_permalink();?>">Leer Más</a>
                        </div>
                    </div>
                    <?php endwhile; endif;?>
