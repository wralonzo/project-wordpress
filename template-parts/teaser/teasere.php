<?php
        // teaser simple. se deben modificar los parametros post type, tag, showpost. también se pueden cambiar por otros.
        $args = array(
            'post_type' => 'post',
            'ID' => 28,
            'showposts' => 5);
        $category_posts = new
            WP_Query($args);
        if ( have_posts() ) :
            while ( $category_posts->have_posts() ) :
                $category_posts->the_post();
                $thumbnail_id   = get_post_thumbnail_id();
                $thumbnail_url  = wp_get_attachment_image_src( $thumbnail_id, 'teaser-e', true );
                $thumbnail_meta = get_post_meta( $thumbnail_id, '_wp_attatchment_image_alt', true );
    ?>
        <div class="teaser teaser-e">
            <div class="teaser-image" style="background:#ccc;">
                <a href="#" class="">
                     <img class="img-responsive" src="<?php echo $thumbnail_url[0]; ?>" alt="<?php the_title(); ?>">
                </a>
            </div>
            <div class="teaser-caption">
                <div class="teaser-text text-center">
                    <h2 class="title">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h2>
                   </div>
            </div>

        </div>
    <?php endwhile; endif;?>
