<section class="col-sm-12 col-md-12 col-lg-12">
    <?php
        $args = array(
            'post_type' => 'post',
            'category_name' => 'noticias',
            'showposts' => 3);
        $category_posts = new
            WP_Query($args);
    ?>
    <div id="carousel" class="carousel carousel-b slide" data-ride="carousel" data-interval="2000">
        <ol class="carousel-indicators">
            <?php if($category_posts->have_posts()) : while($category_posts->have_posts()) : $category_posts->the_post();?>
                <li data-target="#carousel" data-slide-to="<?php echo $category_posts->current_post; ?>" class="<?php if ( $category_posts->current_post == 0 ) : ?>active<?php endif; ?>"></li>
            <?php endwhile;endif;?>
        </ol>
        <!-- rewind loop back to zero without losing data-->
        <?php rewind_posts(); ?>
        <div class="carousel-inner">
            <?php
                if ( have_posts() ) :
                    while ( $category_posts->have_posts() ) :
                        $category_posts->the_post();
                        $thumbnail_id   = get_post_thumbnail_id();
                        $thumbnail_url  = wp_get_attachment_image_src( $thumbnail_id, 'carousel', true );
                        $thumbnail_meta = get_post_meta( $thumbnail_id, '_wp_attatchment_image_alt', true );
            ?>
                <div class="carousel-item <?php if ( $category_posts->current_post == 0 ) : ?>active<?php endif; ?>">
                    <a href="<?php the_permalink(); ?>">
                        <?php if ($thumbnail_id): ?>
                                <img class="d-block carousel-image" src="<?php echo $thumbnail_url[0]; ?>" alt="<?php the_title(); ?>">
                            <?php else : ?>
                                <img class="d-block carousel-image" src="<?php echo get_template_directory_uri() . '/inc/assets/img/carousel-default-background.png'; ?>" alt="<?php the_title(); ?>">
                            <?php endif;?>
                            <div class="carousel-caption">
                                <h3 class="carousel-title">
                                    <?php echo excerpt_limit_words(get_the_title(), '10'); ?>
                                </h3>
                                <p><?php echo excerpt_limit_words(get_the_excerpt(), '25'); ?></p>
                            </div>

                    </a>
                </div>
            <?php endwhile;endif;?>
        </div>
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section><!-- #primary -->

  <!-- ======= Noticias ======= -->
        <div class="row">

                <?php
                    // teaser simple. se deben modificar los parametros post type, tag, showpost. también se pueden cambiar por otros.
                    $args = array(
                        'post_type' => 'post',
                        'category_name' => 'noticias',
                        'showposts' => 6);
                    $category_posts = new
                        WP_Query($args);
                    if ( have_posts() ) :
                        while ( $category_posts->have_posts() ) :
                            $category_posts->the_post();
                            $thumbnail_id   = get_post_thumbnail_id();
                            $thumbnail_url  = wp_get_attachment_image_src( $thumbnail_id, 'news', true );
                            $thumbnail_meta = get_post_meta( $thumbnail_id, '_wp_attatchment_image_alt', true );
                ?>
                <section class="content-area col-sm-12 col-md-12 col-lg-4">
                    <div class="teaser-item teaser-b">
                        <?php if ($thumbnail_id): ?>
                        <a href="#" class="teaser-image">
                            <img class="img-responsive" src="<?php echo $thumbnail_url[0]; ?>" alt="<?php the_title(); ?>">
                        </a>
                        <?php endif;?>
                        <h2 class="teaser-title">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h2>
                        <div class="teaser-excerpt">
                            <?php the_excerpt(); ?>
                            <a class="btn btn-secondary" href="<?php the_permalink();?>">Leer Más</a>
                        </div>
                    </div>
                 </section>
                <?php endwhile; endif;?>
                    </div>


        <!-- /noticias -->
