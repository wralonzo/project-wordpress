 <?php
    // Teaser A
    $args = array(
        'post_type' => 'post',
        'category_name' => 'block',
        'showposts' => 5); // habilitar si no se usa
        //'posts_per_page' => 10,
        //'paged' => $paged,);
    $category_posts = new
        WP_Query($args);
    if ( have_posts() ) :
        while ( $category_posts->have_posts() ) :
            $category_posts->the_post();
            $thumbnail_id   = get_post_thumbnail_id();
            $thumbnail_url  = wp_get_attachment_image_src( $thumbnail_id, 'teaser-abcf', true );
            $thumbnail_meta = get_post_meta( $thumbnail_id, '_wp_attatchment_image_alt', true );
    ?>
        <?php if ($thumbnail_id): ?>
            <div class="teaser teaser-d-i">
                <div class="teaser-text">
                    <h2 class="title">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h2>
                    <?php the_excerpt(); ?>
                    <a class="btn btn-secondary btn-sm" href="<?php the_permalink();?>">Leer Más</a>
                </div>
                    <div class="teaser-image">
                        <a href="<?php the_permalink();?>" class="">
                             <img class="img-responsive" src="<?php echo $thumbnail_url[0]; ?>" alt="<?php the_title(); ?>">
                        </a>
                    </div>
            </div>
        <?php else : ?>
            <div class="teaser teaser-d-i">
                <div class="teaser-text-woi">
                    <h2 class="title">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h2>
                    <?php the_excerpt(); ?>
                    <a class="btn btn-secondary btn-sm" href="<?php the_permalink();?>">Leer más</a>
                </div>
            </div>
        <?php endif;?>

<?php endwhile; endif;?>
